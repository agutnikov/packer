<?php

if (empty($argv[1])) {
    die("Usage: virtual.php fileName\n");
}

$filename = $argv[1];
$lang = 'dev';
if (preg_match('#/home/([a-z0-9_.]+)/#', __FILE__, $matches)) {
    $developer = $matches[1];
} else {
    $developer = basename(dirname(dirname(__DIR__)));
}
$project_root = "/home/$developer/badoo/";

require $project_root . "_deploy/UimagesFileProcess.php";
$Process = new UimagesFileProcess($project_root . "/images/");
echo $Process->getVirtualFileContents($filename, $lang);

function mdie($msg)
{
	die(json_encode($msg));
}
