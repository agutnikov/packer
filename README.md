# What is Packer #

Packer is a bundler tool for js and css files on Badoo. Its core part is ported from an existing UImagesFileProcess.php utility, but with node js and extensibility in mind.

# Getting up and running #

Fix your projects **.realsync** file by adding an exclude for **debug_js.phtml** file and restart realsync, so changes will take effect.


```
##
## dkLab RealSync configuration file.
##

...

# Pathname wildcards to be excluded from the replication.
# Use "*" for any filename character and "**" for any character,
# including "/" in pathnames.

exclude = debug_js.phtml

...
```


Clone repository to your local project directory( For ex: ~/work/badoo ):
```
cd ~/work/badoo
git clone https://agutnikov@bitbucket.org/agutnikov/packer.git
```

Install package dependencies:
```
cd packer
npm i
```

Replace **debug_js.phtml** that is in your d3 project location ( normally is **/home/you/badoo** ) with that is in packer repository 
```
scp php/debug_js.phtml www1.d3:~/badoo/www
```

Create a directory for cached virtual files in your d3 home directory
```
ssh www1.d3 "mkdir .vCache"
```

To verify that everything works as expected - go to your browser and check if javascript files headers changed
to something similar to:
```
/*
 *	Bundle name: -/-/js/hon_v3/base-app.js
 *	Packed in 842 ms.
 */
```

# Running development mode with source maps support #

Before running in devmode, please install protobuf package. See https://wiki.badoojira.com/pages/viewpage.action?pageId=35521612 for detailed instructions

To run development mode use **devmode** script. Use **--developer** parameter to set your d3 account
```
./bin/devmode --developer=agutnikov
```

*This maby a good idea to set an alias in your shell config script ( .bashrc or similar ), so you can run devmode at any time without addressing a script by its fullpath.*
```
alias devmode="/Users/agutnikov/work/badoo/packer/bin/devmode --developer=agutnikov"
```

# How to revert back to UImagesFileProcess #

1. Remove ( or comment ) exclusion for debug_js.phtml from .realsync file
1. Restart realsync so it reload changed config file
1. Do any change (like adding a space) to debug_js.phtml file so realsync will push it to d3 and replace packer version with standard
1. Replace previous change from debug_js.phtml to not commit it ocasionally


# Testing #

To test bundler output, run **test** script in your packer directory **on d3 server**:
```
~/badoo/packer/bin/test
```

Testing script will compare UImagesFileProcess and packer output for a number of
entrypoints and report if any errors found:


```
-/-/js/hon_v3/new-version/page.profile-view.js - OK
-/-/js/hon_v3/new-version/page.profile-edit.js - Error: 
Error: Too deep includes
    at processFile (/home/agutnikov/packer/lib/packer.js:38:10)
    at /home/agutnikov/packer/lib/packer.js:62:20
    at Array.forEach (native)
    at processIncludes (/home/agutnikov/packer/lib/packer.js:49:9)
    at processFile (/home/agutnikov/packer/lib/packer.js:40:14)
    at /home/agutnikov/packer/lib/packer.js:62:20
    at Array.forEach (native)
    at processIncludes (/home/agutnikov/packer/lib/packer.js:49:9)
    at processFile (/home/agutnikov/packer/lib/packer.js:40:14)
    at /home/agutnikov/packer/lib/packer.js:62:20
-/-/js/hon_v3/new-version/page.popularity.js - OK
-/-/js/hon_v3/new-version/page.favorites.js - OK
-/-/js/hon_v3/new-version/page.fans.js - OK
-/-/js/hon_v3/new-version/page.encounters.js - OK
-/-/js/hon_v3/new-version/page.credits.js - OK
-/-/js/hon_v3/new-version/page.empty-photos.js - OK
-/-/js/hon_v3/new-version/page.big-photo-view.js - OK
-/-/js/hon_v3/generic-app.js - OK
-/-/js/help.js - OK
-/-/js/hon_v3/base-app.js - OK
-/-/js/hon_v3/base-lite.js - OK
```


### Who do I talk to? ###

* a.gutnikov@corp.badoo.com