var exec = require('child_process').exec;
var rimraf = require('rimraf').sync;

var generate = [
	'cd ~',
	'mkdir -p ./virtual',
	'~/badoo/packer/bin/virtual-cache --projectDir=~/badoo --cacheDir=./virtual',
	'tar zcf virtual.tar.gz ./virtual'
].join(' && ');

var extract = [
	'cd %dir%',
	'tar zxf virtual.tar.gz'
].join(' && ');

function scp(remotePath, localPath, fnReady) {
	exec('scp www1.d3:' + remotePath + ' ' + localPath, function(error) {
		if (error) {
			throw new Error(error);
		}
		fnReady();
	});
}

function ssh(cmd, fnReady) {
	exec('ssh www1.d3 \'' + cmd + '\'', function(error, stdout) {
		if (error) {
			throw new Error(error);
		}
		fnReady(stdout);
	});
}

function fetch(dir, onReady) {
	ssh(generate, function() {
		scp('~/virtual.tar.gz', dir, function() {
			exec(extract.replace(/%dir%/, dir), function() {
				onReady();
			});
		});
	});
}

module.exports = fetch;
