// This processes devel lines to get the same output as php version
// Could be simplified as soon as php-comparison tests will be removed
var RX_DEV_INCLUDE = /\n[ \t]*(?:\/\/)?[ \t]*;;;[ \t]*(?:\/\/)?[ \t]*(#include)/g;

// remove marker and line part after it
var RX_REMOVE_DEV_LINES = /;;;[^\n]*/g;

// {Boolean} options.includeDevelLines - if true, will keep devel lines
module.exports = function(contents, options) {
	if (!options.includeDevelLines) {
		return contents.replace(RX_REMOVE_DEV_LINES,'');
	} else {
		return contents.replace(RX_DEV_INCLUDE,'$1');
	}
};
