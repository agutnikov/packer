var chokidar = require('chokidar');
var log = require('./utils').log;

var SMALL_BATCH_SIZE = 2;
var BATCH_DELAY_SHORT = 200;
var BATCH_DELAY_LONG = 2000;

function runFsWatcher(glob, options, updateBatchHandler) {
	var lastUpdate;
	var watcher = chokidar.watch(glob, options).on('ready', onReady);

	log('@cyan([Watcher]) Starting fs watcher...');

	function onReady() {
		var batch = [];

		watcher.on('all', function onUpdate(event, filePath) {
			log('@cyan([Watcher]) %s [@cyan(%s)]', filePath, event);
			batch.push({
				event: event,
				filePath: filePath
			});
			lastUpdate = Date.now();
		});

		function nextBatchCheck() {
			// to many files updated? seems like a checked out a branch or something.
			// wait a little bit more...
			if (batch.length >= SMALL_BATCH_SIZE &&
				Date.now() - lastUpdate < BATCH_DELAY_LONG / 2) {
				setTimeout(nextBatchCheck, BATCH_DELAY_LONG);
				return;
			}

			if (batch.length > 0) {
				try {
					updateBatchHandler(batch);
				} catch (Error) {
					log('@red([Watcher]) %s', Error.message);
				}
				batch = [];
			}
			setTimeout(nextBatchCheck, BATCH_DELAY_SHORT);
		}
		nextBatchCheck();
		log('@cyan([Watcher]) Press @red(Ctr + C) to stop');
	}
	return watcher;
}

module.exports = runFsWatcher;
