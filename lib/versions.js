var path = require('path');

var versionMarkers = [
	{
		type: 'full',
		rx: /([^A-Za-z_\-\.])full_version\((.*?)\)/g
	},
	{
		type: 'version',
		rx: /([^A-Za-z_\-\.])version\((.*?)\)/g
	},
	{
		type: 'url',
		rx: /([^A-Za-z_\-\.])url\((.*?)\)/g
	}
];

module.exports = function(contents, options, resource) {
	var env = this;
	versionMarkers.forEach(function(marker) {
		if (marker.type === 'url' &&
			(resource.type !== 'css' || options.noUrlAutoversion)) {
			return;
		}
		contents = contents.replace(marker.rx, function(mt, openerSym, gFileName) {
			var versionFileName = trimUrl(gFileName);
			if (marker.type === 'url' && isAbsoluteUrl(versionFileName)) {
				return openerSym + 'url(' + gFileName + ')';
			}
			var resolved = env.resolveResource(versionFileName, resource.path);
			var result = openerSym;
			if (marker.type === 'url') {
				result += 'url(';
			}
			var direction = resolved.direction;
			if (!direction) {
				direction = resource.direction;
			}
			result += makeVersionedUrl(resolved.path, options, marker.type, direction);
			if (marker.type === 'url') {
				result += ')';
			}
			return result;
		});
	});
	return contents;
};

// TODO: handle production versions too
function makeVersionedUrl(fileName, options, type, direction) {
	if (type === undefined) {
		type = 'version';
	}
	if (path.extname(fileName) === '.css') {
		fileName = ensureCssSuffix(fileName, direction);
	}

	var version = '.versioned';
	var fileNameParts = fileName.split('.');
	fileNameParts[fileNameParts.length - 2] += version;

	var prefix = type === 'full' ? options.fullCdnPrefix : options.cdnPrefix;
	if (options.urlPrefix) {
		prefix += options.urlPrefix;
	}
	return prefix + fileNameParts.join('.');
}

function ensureCssSuffix(fileName, direction) {
	var fileNameParts;
	if (direction && fileName.match(/-(rtl|ltr)\.css/) === null) {
		if (!direction) {
			throw new Error('Internal error: no RTL options are provided ' +
				'but autogen suffix requested');
		}
		fileNameParts = fileName.split('.');
		fileNameParts[fileNameParts.length - 2] += ('-' + direction);
		fileName = fileNameParts.join('.');
	}
	return fileName;
}

function isAbsoluteUrl(url) {
	url = trimUrl(url);
	return url[0] === '/' || url[0] === '#' || url.match(/^[a-z]+:/);
}

function trimUrl(url) {
	return url.replace(/['"\s]+/g, '');
}
