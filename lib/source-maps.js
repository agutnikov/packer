var jsTokens = require('js-tokens');
var sourceMap = require('source-map');

function createSourceMapping(filePath, sourceRoot) {

	var generator = new sourceMap.SourceMapGenerator({
		file: filePath,
		//A root for all relative script paths
		sourceRoot: sourceRoot
		// As a performance option
		//,skipValidation: true
	});

	function addLineMapping(lineMapping) {
		var tokens = lineMapping.src.match(jsTokens);
		var column = 0;
		tokens.forEach(function(token) {
			generator.addMapping({
				generated: {
					line: lineMapping.lineNumGenerated,
					column: column + lineMapping.colNumGenerated
				},
				original: {
					line: lineMapping.lineNumOriginal,
					column: column
				},
				source: lineMapping.file
			});
			column += token.length;
		});
		return this;
	}

	function toJSON() {
		return generator.toJSON();
	}

	return {
		toJSON: toJSON,
		addLineMapping: addLineMapping
	};
}

module.exports = createSourceMapping;
