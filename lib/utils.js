var fs = require('fs');
var path = require('path');
var chalk = require('chalk');
var rimraf = require('rimraf').sync;
var mkdir = require('mkdirp').sync;

function resolveDir(dirPath, checkExist) {
	if (!dirPath) {
		return null;
	}
	var resolved = null;
	if (path.isAbsolute(dirPath)) {
		resolved = dirPath;
	} else if (dirPath[0] === '~') {
		resolved = process.env.HOME + dirPath.slice(1);
	} else {
		resolved = path.resolve(dirPath);
	}
	if (checkExist && !fs.existsSync(resolved)) {
		return null;
	}
	return resolved;
}

function getD3AccountName() {
	var developer;
	var match = module.filename.match(/\/home\/([^\/]*)/);
	if (match) {
		developer = match[1];
	}
	return developer;
}

function log() {
	var args = [].slice.call(arguments);
	var format = args.shift();
	var chalked = format.split(/(@[^\(]*\([^\)]*\))/).reduce(function(parts, part) {
		var parsed;
		if (part[0] === '@') {
			parsed = part.match(/^@([^\()]*)\(([^\)]*)\)/);
			parts.push(chalk[parsed[1]](parsed[2]));
		} else {
			parts.push(part);
		}
		return parts;
	}, []).join('');
	args.unshift(chalked);
	console.log.apply(console, args);
}

function recreateDir(dir) {
	rimraf(dir);
	mkdir(dir);
}

module.exports = {
	resolveDir: resolveDir,
	getD3AccountName: getD3AccountName,
	log: log,
	rmdir: rimraf,
	mkdir: mkdir,
	recreateDir: recreateDir
};
