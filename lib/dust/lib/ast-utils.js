(function () {
	var reader = require('./reader');

	var astUtils = module.exports = {
		/**
		 *
		 * @param ast
		 * @param fileName
		 * @return {*}
		 */
		filter: function (ast, fileName)
		{
			return filterNode({
				fileStack: [
					fileName
				]
			}, ast);
		},

		/**
		 *
		 * @param nodes
		 * @param path
		 * @return {*}
		 */
		extractSections: function (nodes, path)
		{
			var ast,
				block,
				parts = path.split(','),
				singleBlock = parts.length === 1;

			if (singleBlock) {
				ast = extractSection(nodes, path.split('/'), false);
			} else {
				ast = ['body'];
				for (var i = 0; i < parts.length; i++) {
					block = extractSection(nodes, parts[i].split('/'), true);
					block[1][1] = parts[i];
					ast = ast.concat([block]);
				}
			}

			return ast;
		}
	};

	var filters = {
		body: function (context, node)
		{
			var out = [node[0]], i = 1, res, memo;

			while (i < node.length) {
				if (node[i][0] === '*') {
					replaceNode(node, i, includeNode(context, node[i]));
				}

				res = null;

				if (context.substitutions) {
					if (node[i][0] === 'reference' && node[i][1].text in context.substitutions) {
						res = context.substitutions[node[i][1].text];
					} else if (node[i][0] === '??') {
						replaceIfDefined(node, i, context.substitutions[node[i][1].text] || false);
						continue;
					}
				}

				if (!res) {
					res = [filterNode(context, node[i])];
				}

				for (var j = 0; j < res.length; j++) {
					memo = compactBuffers(res[j], out, memo);
				}

				i++;
			}

			return out;
		},

		reference: visit,
		'#':       visit,
		'?':       visit,
		'^':       visit,
		'<':       visit,
		'+':       visit,
		'@':       visit,
		partial:   visit,
		context:   visit,
		params:    visit,
		block:     visit,
		bodies:    visit,
		param:     visit,
		buffer:    noop,
		format:    noop, // disable white space suppression completely
		filters:   noop,
		key:       noop,
		path:      noop,
		literal:   noop,
		raw:       noop,
		comment:   nullify,
		line:      nullify,
		col:       nullify,
		'*':	   nullify, // include
		'??':      nullify // precompiler if
	};

	function compactBuffers (res, out, memo) {
		if (res) {
			if (res[0] === 'buffer' || res[0] === 'format') {
				if (out.length === 1 || out[out.length - 1][0] !== 'buffer') {
					out.push(['buffer', '']);
				}

				out[out.length - 1][1] += res[1];
			} else {
				out.push(res);
			}
		}

		return memo;
	}

	function filterNode (context, node) {
		return filters[node[0]](context, node);
	}

	function replaceNode (node, index, newNodes) {
		if (newNodes) {
			node.splice.apply(node, [index, 1].concat(newNodes));
		} else {
			node.splice(index, 1);
		}
	}

	function replaceIfDefined (node, index, nodeValue) {
		var bodyNodes = node[index][4], cases = {}, selectedCase;

		for (var i = 1; i < bodyNodes.length; i++) {
			var block = bodyNodes[i];
			cases[block[1][1]] = block[2].slice(1);
		}

		if (nodeValue !== false && nodeValue[0][0] === 'buffer') {
			selectedCase = cases[nodeValue[0][1]];
		}

		if (!selectedCase) {
			selectedCase = cases[nodeValue  ? 'block' : 'else'];
		}

		replaceNode(node, index, selectedCase);
	}


	function includeNode (context, node) {
		var i, name, type, param, value,
			substitutions = {},
			includeName = node[1][1],
			paramsNode = node[3],
			bodiesNode = node[4];

		for (i = 1; i < paramsNode.length; i++) {
			param = paramsNode[i];
			name = param[1][1];
			type = param[2][0];

			switch (type) {
				case 'literal':
					value = ['buffer', param[2][1].toString()];
					break;

				case 'key':
					value = ['reference', param[2], ['filters']];
					break;

				default:
					throw new Error('Unknown type of param [' + name + '] in include [' + includeName + ']');
			}

			substitutions[name] = [value];
		}

		for (i = 1; i < bodiesNode.length; i++) {
			param = bodiesNode[i];
			name = param[1][1];
			type = param[3][0];

			if (name === 'block') {
				name = 'content'; // Name for $default variable.
			}



			if (type !== 'body') {
				throw new Error('Unknown type of body [' + name + '] in include [' + includeName + ']');
			}



			substitutions[name] = param[3].slice(1);
		}

		includeName = includeName.replace(/\./g, '/');

		var includeFileName = reader.getIncludeFileName(context.fileStack[0], includeName);

		if (context.fileStack.indexOf(includeFileName) >= 0) {
			throw new Error('Circular dependency detected for include [' + includeName + ']');
		}

		try {
			var ast = filterNode({
				fileStack: [includeFileName].concat(context.fileStack),
				substitutions: substitutions
			}, reader.read(includeFileName));
		} catch (err) {
			err.includeStack = '/' + includeName + (err.includeStack || '');

			if (node[5]) {
				err.line = node[5][1];
			}

			throw err;
		}

		ast.shift();

		return ast;
	}

	function extractSection(nodes, path, keepDefinition) {
		// section ast structure
		// [#, [key, <SECTION_NAME>], [context], [params], [bodies, [param, [literal, block], <INNER_BODY>]]]
		var i, node, current;

		// getting name of the first section in the path
		current = path.shift();

		for (i = 1; i < nodes.length; i++) {
			// looping though nodes, each node is an array where first item is node type and the rest is sub nodes
			node = nodes[i];

			if (node[0] === '#' && node[1][1] === current) {
				if (keepDefinition && !path.length) {
					return node;
				}

				node = node[4/*bodies*/];

				if (!node[1/*param*/]) {
					throw new Error('Block [' + current + '] has no body');
				}

				if (node.length > 2) {
					throw new Error('Block [' + current + '] has too many chunks [' + node.length + ']');
				}

				if (node[1/*param*/][1/*literal*/][1] !== 'block') {
					throw new Error('Block [' + current + '] has wrong type [' + node[1/*param*/][1/*literal*/][1] + ']');
				}

				node = node[1/*param*/][2/*body*/];

				return path.length ? extractSection(node, path, keepDefinition) : node;
			}
		}

		throw new Error('Block [' + current + '] is not found');
	}

	function visit(context, node) {
		var out = [node[0]],
			i, len, res;
		for (i=1, len=node.length; i<len; i++) {
			res = filterNode(context, node[i]);
			if (res) {
				out.push(res);
			}
		}
		return out;
	}

	function noop(_, node) {
		return node;
	}

	function nullify(){}
})();
