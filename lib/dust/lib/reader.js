(function () {
	var fs = require('fs'),
		path = require('path'),
		blitzUtils = require('./blitz-utils'),
		parser = require('./parser');

	var searchPaths = [],
		templatesDir = path.resolve(__dirname + '/..') + '/',
		templateExt = ['.dust.html', '.tpl'];

	module.exports = {
		/**
		 *
		 * @param paths
		 */
		setSearchPaths: function (paths)
		{
			searchPaths = paths
		},

		/**
		 *
		 * @param dir
		 */
		setTemplatesDir: function (dir)
		{
			templatesDir = dir;
		},

		/**
		 * Resolves relative path to absolute path.
		 * @param relative
		 * @returns {string}
		 */
		resolvePath: function (relative)
		{
			if (relative.indexOf('/') === 0) {
				return relative;
			}

			return path.resolve(templatesDir + relative);
		},

		/**
		 *
		 * @param currentTemplateFileName
		 * @param includeName
		 * @return {string}
		 */
		getIncludeFileName: function (currentTemplateFileName, includeName)
		{
			var i, j, fileName,
				currentWorkingDirectory = path.resolve(path.dirname(currentTemplateFileName)) + '/';

			for (j = 0; j < templateExt.length; j++) {
				fileName = currentWorkingDirectory + includeName + templateExt[j];
				if (fs.existsSync(fileName)) {
					return fileName;
				}
			}

			for (i = 0; i < searchPaths.length ; i++) {
				for (j = 0; j < templateExt.length; j++) {
					fileName = templatesDir + searchPaths[i] + includeName + templateExt[j];
					if (fs.existsSync(fileName)) {
						return fileName;
					}
				}
			}

			throw new Error('Include [' + includeName + '] not found. Search for [' + searchPaths.join(':') + '] in [' + templatesDir + ']');
		},

		/**
		 *
		 * @param fileName
		 */
		read: function (fileName)
		{
			var source = fs.readFileSync(fileName, 'utf-8');

			source = source.replace(/{![!#]?[0-9]+}/g, ''); // remove lexem markers

			// blitz template compatibility
			var isBlitz = fileName.substr(-4) === '.tpl';
			if (isBlitz) {
				source = blitzUtils.convertToDust(source);
			}

			source = source.
				replace(/^[\s\uFEFF\xA0]+|[\s\uFEFF\xA0]+$/g, ''). // trim
				replace(/\n+/g,'\n'); // fold empty lines

			return parser.parse(source);
		},

		utf8Len: function (str) {
			var len = 0, l = str.length;

			for (var i = 0; i < l; i++) {
				var c = str.charCodeAt(i);
				if (c <= 0x0000007F) len++;
				else if (c >= 0x00000080 && c <= 0x000007FF) len += 2;
				else if (c >= 0x00000800 && c <= 0x0000FFFF) len += 3;
				else len += 4;
			}

			return len;
		}
}
})();
