(function() {
	var reader = require('./reader'),
		astUtils = require('./ast-utils'),
		compileToJavascript = require('./compile-to-javascript');

	module.exports = {
		/**
		 *
		 * @param target
		 * @param fileName
		 * @param templateName
		 * @param extractBlockPath
		 * @return {*}
		 */
		compile: function (target, fileName, templateName, extractBlockPath)
		{
			if (!templateName && templateName !== null) {
				throw new Error('Template name missed');
			}

			// Foundation for backward compilation to blitz
			if (target !== 'javascript') {
				throw new Error('Wrong compile target [' + target + ']');
			}

			var ast = astUtils.filter(reader.read(fileName), fileName);

			// extract sections
			if (extractBlockPath) {
				ast = astUtils.extractSections(ast, extractBlockPath);
			}

			return compileToJavascript.compile(ast, templateName);
		}
	};
})();
