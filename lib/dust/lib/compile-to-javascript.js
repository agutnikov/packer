(function () {
	var escape = JSON.stringify;

	module.exports = {
		compile: function (ast, name) 
		{
			var context = {
				name: name,
				bodies: [],
				bodiesParams: [],
				blocks: {},
				sections: {},
				index: 0,
				parentIndex: [0]
			};

			return '(function(){$u.dust.register(' +
				(name ? '"' + name + '"' : 'null') + ',' +
				compileNode(context, ast) +
				');' +
				compileBlocks(context) +
				compileBodies(context) +
				'return body_0;' +
				'})();';
		}
	};

	var nodes = {
		body: function (context, node) 
		{
			var id = context.index++, name = 'body_' + id;
			context.bodies[id] = compileParts(context, node);
			return name;
		},

		buffer: function (context, node) 
		{
			return '.write(' + escape(node[1]) + ')';
		},

		format: function (context, node) 
		{
			return '.write(' + escape(node[1] + node[2]) + ')';
		},

		reference: function (context, node) 
		{
			return '.reference(' + escape(node[1].text) +
				',ctx' + compileNode(context, node[2]) + ')';
		},

		'#': function (context, node) 
		{
			return compileSection(context, node, 'section');
		},

		'?': function (context, node) 
		{
			return compileSection(context, node, 'exists');
		},

		'^': function (context, node) 
		{
			return compileSection(context, node, 'notexists');
		},

		'<': function (context, node) 
		{
			var bodies = node[4];
			for (var i = 1, len = bodies.length; i < len; i++) {
				var param = bodies[i],
					type = param[1][1];
				if (type === 'block') {
					context.blocks[node[1].text] = compileNode(context, param[2]);
					return '';
				}
			}
			return '';
		},

		'+': function (context, node) 
		{
			if (typeof(node[1].text) === 'undefined' && typeof(node[4]) === 'undefined') {
				return '.block(ctx.getBlock(' +
					compileNode(context, node[1]) +
					',chk, ctx),' + compileNode(context, node[2]) + ', {},' +
					compileNode(context, node[3]) +
					')';
			} else {
				return '.block(ctx.getBlock(' +
					escape(node[1].text) +
					'),' + compileNode(context, node[2]) + ',' +
					compileNode(context, node[4]) + ',' +
					compileNode(context, node[3]) +
					')';
			}
		},

		'@': function (context, node) 
		{
			return '.helper(' +
				escape(node[1].text) +
				',' + compileNode(context, node[2]) + ',' +
				compileNode(context, node[4]) + ',' +
				compileNode(context, node[3]) +
				')';
		},

		partial: function (context, node) 
		{
			return '.partial(' +
				compileNode(context, node[1]) +
				',' + compileNode(context, node[2]) +
				',' + compileNode(context, node[3]) + ')';
		},

		context: function (context, node) 
		{
			if (node[1]) {
				return 'ctx.rebase(' + compileNode(context, node[1]) + ')';
			}
			return 'ctx';
		},

		params: function (context, node) 
		{
			var out = [];
			for (var i = 1, len = node.length; i < len; i++) {
				out.push(compileNode(context, node[i]));
			}
			if (out.length) {
				return '{' + out.join(',') + '}';
			}
			return '{}';
		},

		bodies: function (context, node) 
		{
			var out = [];
			for (var i = 1, len = node.length; i < len; i++) {
				out.push(compileNode(context, node[i]));
			}
			return '{' + out.join(',') + '}';
		},

		block: function (context, node) {
			if (node[2].length > 1) {
				context.bodiesParams[context.index] = compileNode(context, node[2]);
			}

			var name = compileNode(context, node[1]),
				out = name + ':' + compileNode(context, node[3]);

			return out;
		},

		param: function (context, node) 
		{
			return compileNode(context, node[1]) + ':' + compileNode(context, node[2]);
		},

		filters: function (context, node) 
		{
			var list = [];
			for (var i = 1, len = node.length; i < len; i++) {
				var filter = node[i];
				list.push('"' + filter + '"');
			}
			return (list.length ? ',[' + list.join(',') + ']' : '');
		},

		key: function (context, node) 
		{
			return 'ctx.get(["' + node[1] + '"], false)';
		},

		path: function (context, node) 
		{
			var current = node[1],
				keys = node[2],
				list = [];

			for (var i = 0, len = keys.length; i < len; i++) {
				if (Array.isArray(keys[i])) {
					list.push(compileNode(context, keys[i]));
				} else {
					list.push('"' + keys[i] + '"');
				}
			}
			return 'ctx.getPath(' + current + ', [' + list.join(',') + '])';
		},

		literal: function (context, node) 
		{
			return escape(node[1]);
		},
		
		raw: function (context, node) 
		{
			return ".write(" + escape(node[1]) + ")";
		}
	};

	function compileNode(context, node) {
		return nodes[node[0]](context, node);
	}

	function compileBlocks(context) {
		var out = [],
			blocks = context.blocks,
			name;

		for (name in blocks) {
			out.push('"' + name + '":' + blocks[name]);
		}
		if (out.length) {
			context.blocks = 'ctx=ctx.shiftBlocks(blocks);';
			return 'var blocks={' + out.join(',') + '};';
		}
		return context.blocks = '';
	}

	function compileBodies(context) {
		var out = [],
			bodies = context.bodies,
			bodiesParams = context.bodiesParams,
			blx = context.blocks,
			i, j, len, subsections;

		for (i = 0, len = bodies.length; i < len; i++) {
			out[i] = 'function body_' + i + '(chk,ctx){' +
			blx + 'return chk' + bodies[i] + ';};';

			if (bodiesParams[i]) {
				out[i] += 'body_' + i + '.params=function(ctx){return ' + bodiesParams[i] + '};';
			}


			subsections = context.sections[i];
			if (subsections) {
				out[i] += 'body_' + i + '.s={';
				for (j = 0; j < subsections.length; j++) {
					out[i] += '\'' + subsections[j][0] + '\':body_' + subsections[j][1];
					if (j !== subsections.length - 1) out[i] += ',';
				}
				out[i] += '};';
			}
		}
		return out.join('');
	}

	function compileParts(context, body) {
		var parts = '',
			i, len;
		for (i = 1, len = body.length; i < len; i++) {
			parts += compileNode(context, body[i]);
		}
		return parts;
	}

	function compileSection(context, node, cmd) {
		var currentIndex = context.index,
			parentIndex = context.parentIndex[context.parentIndex.length - 1];

		var sections = context.sections[parentIndex] = context.sections[parentIndex] || [];
		sections.push([node[1][1], context.index]);

		context.parentIndex.push(currentIndex);

		var out = '.' + cmd + '(' +
			compileNode(context, node[1]) +
			',' + compileNode(context, node[2]) + ',' +
			compileNode(context, node[4]) + ',' +
			compileNode(context, node[3]) +
			')';

		context.parentIndex.pop();

		return out;
	}

})();
