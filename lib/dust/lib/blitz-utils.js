(function() {
	module.exports = {
		ifStack: [],
		flexCounter: [],

		parseFlexArgs: function (value, keyOnly)
		{
			var len = 'isSuitableFlexibleVariant'.length + 1, args = [], strip = /^"?([^"]+)"?$/;

			value = value.substring(len, value.length - 1).replace(/'/g, '"').split(',');

			if(keyOnly) {
				return value[3];
			}

			for (var i = 4; i < value.length; i += 3) {
				var varName = value[i].match(strip)[1];
				args.push(varName + '=' + value[i + 2] + ' $' + varName + '=' + value[i + 1]);
			}

			return ' key=' + value[3] + ' ' + args.join(' ');
		},


		convertToDust: function (source)
		{
			return source.

				replace(/{{\s*(IF|ELSEIF|ELSE|END)\s*([^}]*?)?\s*}}/ig, (function (_, op, value) {
					op = op.toLowerCase();

					switch (op) {
						case 'if':
							if (value === 'IE') {
								this.ifStack.push('ie');
								return '{@ie}';
							} else if (value.indexOf('isSuitableFlexibleVariant') === 0) {
								this.flexCounter.push(1);
								this.ifStack.push('flexible');
								return '{@flexible' + this.parseFlexArgs(value) + '}';
							} else {
								this.ifStack.push(value);
								return '{?' + value + '}';
							}
							break;

						case 'elseif':
							if (value.indexOf('isSuitableFlexibleVariant') === 0) {
								this.ifStack.push('flexible');
								return '{:'+ this.parseFlexArgs(value, true) +'}';
							}
							break;

						case 'else':
							return '{:else}';
							break;

						case 'end':
							var name = this.ifStack.pop();
							if (name === 'flexible') {
								this.flexCounter.pop();
							}

							return '{/' + name + '}';
							break;
					}
				}).bind(this)).

				replace(/{{([\w+|\-]+)}}/ig, function (_, p) {
					var parts = p.split('|'), varname = parts[0], modifier = parts[1];

					if (modifier === 'raw') {
						modifier = 's';
					}

					if (modifier) {
						varname += '|' + modifier;
					}

					return '{' + varname + '}';
				}).

				replace(/<!--\s*(BEGIN|END)\s*(\w+)\s*-->/ig, function (_, p1, p2) {
					var blockname = '';

					if (p1 === 'BEGIN') {
						blockname += '#';
					} else if (p1 === 'END') {
						blockname += '/';
					}

					blockname += p2;

					return '{' + blockname + '}';
				}).

				replace(/{{(IF|END) IE}}/ig, function (_, p) {
					return '{' + (p === 'IF' ? '@' : '/') + 'ie}'
				}).

				replace(/{{((partner_|exclusive_partner_)?version)\(['|"]([^\)]+)['|"]\)}}/ig, function (_, p1, __, p2) {
					return '{@version url="' + p1 + '(' + p2 + ')"/}';
				}).

				replace(/{{WordDeclination::get\((\w+)\s*,\s*\d+\s*,\s*(\d+)\s*,\s*\d+\)}}/ig, function (_, p1, p2) {
					return '{@declination word=' + p1 + ' declination=' + p2 + '/}'
				}).

				replace(/{{WordDeclination::getDeclinableName\(\'(\w+)\'\s*,\s*\'(\w+)\'\s*,\s*(\d+)\s*,\s*\d+\)}}/ig, function (_, p1, p2, p3) {
					return '{@declination_name type="' + p1 + '" word="' + p2 + '" declination="' + p3 + '"/}'
				}).

				replace(/{{WordDeclination::partner\((\w+)\s*,\s*(\d+)\s*,\s*\d+\)}}/ig, function (_, p1, p2) {
					return '{@declination_partner word=' + p1 + ' declination=' + p2 + '/}'
				}).

				replace(/<!--\s*\/?nolexem\s*-->/ig, '');
		}
	};
})();
