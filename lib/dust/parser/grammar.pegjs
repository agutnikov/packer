start
  = body

/*-------------------------------------------------------------------------------------------------------------------------------------
   body is defined as anything that matches with the part 0 or more times
---------------------------------------------------------------------------------------------------------------------------------------*/
body
  = p:part* {
    return ["body"]
           .concat(p)
           .concat([['line', line()], ['col', column()]]);
  }

/*-------------------------------------------------------------------------------------------------------------------------------------
   part is defined as anything that matches with raw or section or partial or reference or buffer
---------------------------------------------------------------------------------------------------------------------------------------*/
part
  = raw / section / reference / lexem_marker / buffer

/*-------------------------------------------------------------------------------------------------------------------------------------
   section is defined as matching with with sec_tag_start followed by 0 or more white spaces plus a closing brace plus body
   plus bodies plus end_tag or sec_tag_start followed by a slash and closing brace
---------------------------------------------------------------------------------------------------------------------------------------*/
section "section"
  = t:sec_tag_start ws* rd b:body e:bodies n:end_tag? &{
    if( (!n) || (t[1].text !== n.text) ) {
      error("Expected end tag for "+t[1].text+" but it was not found.");
    }
    return true;
  }
  {
    e.push(["block", ["literal", "block"], ["params"], b]);
    t.push(e);
    return t.concat([['line', line()], ['col', column()]]);
  }
  / t:sec_tag_start ws* "/" rd {
    t.push(["bodies"]);
    return t.concat([['line', line()], ['col', column()]]);
  }

/*-------------------------------------------------------------------------------------------------------------------------------------
   sec_tag_start is defined as matching an opening brace followed by one of #?^@ plus identifier plus plus param (only for helpers)
   followed by 0 or more white spaces
---------------------------------------------------------------------------------------------------------------------------------------*/
sec_tag_start
  = sec_tag_start_without_params / sec_tag_start_with_params

sec_tag_start_without_params
  = ld t:([#?^][\?]?) ws* n:identifier
  { return [t.join(''), n, ["context"], ["params"]] }

sec_tag_start_with_params
  = ld t:[@\*] ws* n:identifier p:params
   { return [t, n, ["context"], p] }

include_path
  = n:[a-zA-Z0-9/]+ { var key = n.join(''), arr = ["key", key]; arr.text = key; return arr; }

/*-------------------------------------------------------------------------------------------------------------------------------------
   end_tag is defined as matching an opening brace followed by a slash plus 0 or more white spaces plus identifier followed
   by 0 or more white spaces and ends with closing brace
---------------------------------------------------------------------------------------------------------------------------------------*/
end_tag "end tag"
  = ld "/" ws* n:identifier ws* rd
  { return n }

/*-------------------------------------------------------------------------------------------------------------------------------------
   context is defined as matching a colon followed by an identifier
---------------------------------------------------------------------------------------------------------------------------------------*/
context
  = n:(":" n:identifier {return n})?
  { return n ? ["context", n] : ["context"] }

/*-------------------------------------------------------------------------------------------------------------------------------------
  params is defined as matching white space followed by = and identfier or inline
---------------------------------------------------------------------------------------------------------------------------------------*/
params "params"
  = p:(ws+ k:key "=" v:(number / identifier / inline) {return ["param", ["literal", k], v]})*
  { return ["params"].concat(p) }

/*-------------------------------------------------------------------------------------------------------------------------------------
   bodies is defined as matching a opening brace followed by key and closing brace, plus body 0 or more times.
---------------------------------------------------------------------------------------------------------------------------------------*/
bodies "bodies"
  = p:(ld ":" k:(key / ('"' l:literal '"') {return l}) p:params rd v:body {return ["block", ["literal", k], p, v]})*
  { return ["bodies"].concat(p) }

/*-------------------------------------------------------------------------------------------------------------------------------------
   reference is defined as matching a opening brace followed by an identifier plus one or more filters and a closing brace
---------------------------------------------------------------------------------------------------------------------------------------*/
reference "reference"
  = ld n:identifier f:filters rd
  { return ["reference", n, f].concat([['line', line()], ['col', column()]]) }

/*-------------------------------------------------------------------------------------------------------------------------------------
   filters is defined as matching a pipe character followed by anything that matches the key
---------------------------------------------------------------------------------------------------------------------------------------*/
filters "filters"
  = f:("|" n:key {return n})*
  { return ["filters"].concat(f) }

/*-------------------------------------------------------------------------------------------------------------------------------------
   identifier is defined as matching a key only
---------------------------------------------------------------------------------------------------------------------------------------*/
identifier "identifier"
  = k:key  { var arr = ["key", k]; arr.text = k; return arr; }

number "number"
  = n:(float / integer) { return ['literal', n]; }

float "float"
  = l:integer "." r:integer+ { return parseFloat(l + "." + r.join('')); }

integer "integer"
  = digits:[0-9]+ { return parseInt(digits.join(""), 10); }

/*-------------------------------------------------------------------------------------------------------------------------------------
   key is defined as a character matching a to z, upper or lower case, followed by 0 or more alphanumeric characters
---------------------------------------------------------------------------------------------------------------------------------------*/
key "key"
  = h:[a-zA-Z_$] t:[0-9a-zA-Z_$-\.]*
  { return h + t.join('') }

array "array"
  = i:( lb a:( n:([0-9]+) {return n.join('')} / identifier) rb  {return a; }) nk: array_part? { if(nk) { nk.unshift(i); } else {nk = [i] } return nk; }

array_part "array_part"
  = d:("." k:key {return k})+ a:(array)? { if (a) { return d.concat(a); } else { return d; } }

/*-------------------------------------------------------------------------------------------------------------------------------------
   inline params is defined as matching two double quotes or double quotes plus literal followed by closing double quotes or
   double quotes
---------------------------------------------------------------------------------------------------------------------------------------*/

inline "inline"
  = '"' '"'                 { return ["literal", ""].concat([['line', line()], ['col', column()]]) }
  / '"' l:literal '"'       { return ["literal", l].concat([['line', line()], ['col', column()]]) }

/*-------------------------------------------------------------------------------------------------------------------------------------
  inline_part is defined as matching a reference or literal
---------------------------------------------------------------------------------------------------------------------------------------*/
inline_part
  = reference / l:literal { return ["buffer", l] }

buffer "buffer"
  = e:eol w:ws*
  { return ["format", e, w.join('')].concat([['line', line()], ['col', column()]]) }
  / b:(!tag !raw !eol !lexem_marker c:. {return c})+
  { return ["buffer", b.join('')].concat([['line', line()], ['col', column()]]) }

/*-------------------------------------------------------------------------------------------------------------------------------------
   literal is defined as matching esc or any character except the double quotes and it cannot be a tag
---------------------------------------------------------------------------------------------------------------------------------------*/
literal "literal"
  = b:(!tag c:(esc / [^"]) {return c})+
  { return b.join('') }

esc
  = '\\"' { return '"' }

raw "raw"
  = "{`" rawText:(!"`}" char:. {return char})* "`}"
  { return ["raw", rawText.join('')].concat([['line', line()], ['col', column()]]) }

/*-------------------------------------------------------------------------------------------------------------------------------------
   tag is defined as matching an opening brace plus any of #?^:@/~ plus 0 or more whitespaces plus any character or characters that
   doesn't match rd or eol plus 0 or more whitespaces plus a closing brace
---------------------------------------------------------------------------------------------------------------------------------------*/
tag
  = ld ws* [#?^:@/\*] ws* (!rd !eol .)+ ws* rd
  / reference

ld
  = "{"

rd
  = "}"

lb
  = "["

rb
  = "]"

eol
  = "\n"        //line feed
  / "\r\n"      //carriage + line feed
  / "\r"        //carriage return
  / "\u2028"    //line separator
  / "\u2029"    //paragraph separator

ws
  = [\t\v\f \u00A0\uFEFF] / eol

/*--------------------------------------------------------------------------------------------------------------------*/

lexem_marker "lexem"
  = "#lexem_" type:("begin" / "end") "_" id:[0-9]+ "#" {return ['buffer', '<!-- LEXEM_' + type.toUpperCase() + '_' + id.join('') + ' -->']}
