var LTR_BEGIN   = "/* @ltr begin */";
var LTR_END     = "/* @ltr end */";
var RTL_BEGIN   = "/* @rtl begin */";
var RTL_END     = "/* @rtl end */";
var NOFLIP_BEGIN = "/* @noflip begin */";
var NOFLIP_END  = "/* @noflip end */";

module.exports = function(contents, options) {
	if( options.direction === 'ltr' ) {
		return ltr(contents);
	}
};

function ltr(contents) {
	var parts = contents.split(RTL_BEGIN);
	var result = [parts.shift()];
	var endPos;
	parts.forEach(function (part) {
		endPos = part.indexOf(RTL_END);
		if (endPos === -1) {
			return;
		}
		result.push(part.slice(endPos + RTL_END.length));
	});
	return result.join('')
		.replace(LTR_BEGIN,'')
		.replace(LTR_END,'')
		.replace(NOFLIP_BEGIN,'')
		.replace(NOFLIP_END,'');
}
