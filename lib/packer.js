var fs = require('fs');
var path = require('path');
var btoa = require('btoa');
var createEnv = require('./env');
var createSourceMap = require('./source-maps');

var MAX_INCLUDE_DEPTH = 200;
var PROCESSED_TYPES = 'virtual,tpl,js,css'.split(',');
var INCLUDE_BEGIN = '#include "';
var INCLUDE_END = '"';

module.exports = function(name, packOptions, deps) {

	var result = [];
	var includeStack = [];
	var includedFiles = {};
	var env = createEnv(packOptions);

	// source mappings
	var sourceMap;
	var columnOffset = 0;
	var resultLinesCount = 0;
	if (packOptions.sourceMaps) {
		sourceMap = createSourceMap(name, packOptions.sourceRoot);
	}

	// Bundle header
	var header;
	if (packOptions.bundleHeader) {
		header = packOptions.bundleHeader.map(function(line) {
			return ' *\t' + line;
		});
		header.unshift('/*');
		header.push(' */\n');
		writeResultPart(header.join('\n'));
	}

	var tookTime = Date.now();
	processFile(name.replace(/-\/-\/[^\/]*/,''));
	// Join source maps if collected
	if (sourceMap) {
		result.push(sourceMapInlined());
	}
	tookTime = Date.now() - tookTime;
	return result.join('')
		.replace('%bundleName', String(name))
		.replace('%bundleTime', String(tookTime));

	//
	// Processing functions
	//
	function processFile(fileName, parentFileName) {
		var resource = env.getResource(fileName, parentFileName);
		if (!resource || !resource.contents) {
			throw new Error('Error: cannot resolve ' + fileName +
				' from ' + parentFileName);
		}
		var fullPath = resource.dir + resource.path;
		var contents = resource.contents;

		if (includedFiles[fullPath]) {
			writeResultPart(getLegacyInclusionComment(fileName, resource));
			return;
		}

		// if need to save dependencies
		if (deps) {
			deps[resource.path] = true;
		}

		if (PROCESSED_TYPES.indexOf(resource.type) === -1) {
			writeResultPart(contents, resource);
			return;
		}

		includeStack.push(fullPath);
		includedFiles[fullPath] = true;
		if (includeStack.length >= MAX_INCLUDE_DEPTH) {
			throw new Error('Too deep includes. Files = ' + includeStack.join('->'));
		}
		processIncludes(resource);
		includeStack.pop();
	}

	function processIncludes(resource) {
		var parts = resource.contents.split(INCLUDE_BEGIN);
		var lineOffset = 0;

		parts.forEach(function(part, idx) {
			var includeParts;
			var file;
			var rest;
			if (idx > 0) {
				includeParts = part.split(INCLUDE_END);
				if (includeParts.length < 2) {
					writeResultPart(part, resource);
					return;
				}
				file = includeParts.shift();
				rest = includeParts.join(INCLUDE_END);
				processFile(file, resource.path);
			} else {
				rest = part;
			}

			var lines;
			if (sourceMap && shouldMapResource(resource)) {
				// Calculate line offsets for source mapping
				lines = rest.split('\n');
				writeResultPart(rest, resource, {
					lines: lines,
					firstLineColumnOffset: columnOffset,
					lineOffset: lineOffset
				});
				columnOffset = lines[lines.length - 1].length;
				lineOffset += lines.length - 1;
			} else {
				writeResultPart(rest, resource);
			}
		});
	}

	function writeResultPart(part, resource, mapping) {
		result.push(part);
		if (sourceMap) {
			if (mapping) {
				mapping.lines.forEach(function(line, index) {

					sourceMap.addLineMapping({
						// any column offset for first line of part?
						colNumGenerated: index === 0 ? columnOffset : 0,
						// where this line in resulting file
						lineNumGenerated: resultLinesCount + index + 1,
						// where this line in original file
						lineNumOriginal: mapping.lineOffset + index + 1,
						// file name relative to sourceRoot ( see source-maps.js )
						file: resource.path,
						// the line itself
						src: line
					});
				});

				resultLinesCount += mapping.lines.length - 1;
			} else {
				// Just update result lines count
				resultLinesCount += part.split('\n').length - 1;
			}
		}
	}

	function sourceMapInlined() {
		var ext = path.extname(name).slice(1);
		var src;
		if (ext === 'js') {
			src = '\n//@ sourceMappingURL=data:application/json;charset=utf-8;' +
				'base64,' + btoa(JSON.stringify(sourceMap.toJSON()));
		} else if (ext === 'css') {
			src = '\n/*# sourceMappingURL=data:application/json;charset=utf-8;' +
				'base64,' + btoa(JSON.stringify(sourceMap.toJSON())) + '*/';
		}
		return src;
	}

	// Map only non-gpb javascript files
	function shouldMapResource(resource) {
		return (resource.type === 'js' && !resource.isGpb) || resource.type === 'css';
	}

	function getLegacyInclusionComment(fileName, resource) {
		var name = fileName;
		if (resource.isGpb) {
			name = resource.dir + resource.path;
		} else if (resource.type === 'tpl') {
			name = fileName;
		} else {
			name = resource.path;
		}
		return '\n/* ' + name + ' already included before */\n';
	}
};
