var fs = require('fs');
var path = require('path');
var dust = require('./dust/lib/compiler');

// TODO: resource resolver (resolve, getContent)
// TODO: non-resolved error tracking

module.exports = function(options) {

	var projectDir = options.projectDir;
	var lang = options.lang;
	var imagesDir = projectDir + '/images/';
	var tplDir =  projectDir + '/_templates/';
	var gpbJs = options.gpbDir + '/images/-/-/js/';
	var virtualDir = options.virtualDir;

	function resolveResource(name, parentName) {
		var resolved = null;
		var imagesFile;
		var match;
		var direction;
		if (name.match(/^virtual:/)) {
			resolved = {
				path: name,
				type: 'virtual'
			};
		} else if (name.match(/^tpl:/)) {
			name = name.replace(/^tpl:/, '');
			resolved = {
				dir: tplDir,
				path: resolveTemplateFile(tplDir, name, lang),
				type: 'tpl'
			};
		} else if (name.match(/^gpbjs\//)) {
			resolved = {
				dir: gpbJs,
				path: resolveGpbFile(gpbJs, name),
				type: 'js',
				isGpb: true
			};
		} else {
			if (match = name.match(/-(ltr|rtl)\.css/)) {
				name = name.replace(/(-ltr|-rtl).css/, '.css');
				direction = match[1];
			}
			imagesFile = resolveImagesFile(imagesDir, name, parentName, options.lang);
			resolved = {
				dir: imagesDir,
				path: imagesFile,
				type: path.extname(imagesFile).slice(1)
			};
			if (direction) {
				resolved.direction = direction;
			}
		}
		return resolved;
	}

	function getResource(name, parentName) {
		var resource = resolveResource.call(this, name, parentName);
		if (resource) {
			try {
				if (resource.type === 'virtual') {
					resource.contents = getVirtualFileContent(virtualDir, name);

				} else if (resource.type === 'tpl') {
					resource.contents =
						getBlitzFileContents(resource.dir, resource.path, name);

				} else {
					resource.contents =
						fs.readFileSync(resource.dir + resource.path, 'utf-8');
				}
			} catch (Err) {
				if (parentName) {
					throw Error('Error: getting resource ' + name +
						' from ' + parentName + '\n' + Err.stack);
				} else {
					throw Error('Error: getting resource ' + name + '\n' + Err.stack);
				}
			}

			applyProcessors.call(this, resource);
		}
		return resource;
	}

	function applyProcessors(resource) {
		var env = this;
		if (!options.resourceProcessors) {
			return;
		}
		options.resourceProcessors.forEach(function(proc) {
			if (proc.type && proc.type.split(',').indexOf(resource.type) !== -1) {
				resource.contents = proc.process.call(
					env, resource.contents, proc.options, resource);
			}
		});
	}

	return {
		options: options,
		resolveResource: resolveResource,
		getResource: getResource
	};
};

// Gpb
function resolveGpbFile(gpbDir, fileName) {
	var resolved = null;
	if (fs.existsSync(gpbDir + fileName)) {
		resolved = fileName;
	}
	return resolved;
}

// Images
function resolveImagesFile(imagesDir, fileName, parentName, lang) {
	var paths = getImagesIncludePaths(imagesDir, lang);
	if (parentName) {
		paths.unshift(path.dirname(imagesDir + parentName) + '/');
	}
	var resolved = doResolveImagesPath(paths, fileName);
	// find relative path
	if (resolved && resolved.indexOf(imagesDir) !== -1) {
		resolved = path.relative(imagesDir, resolved);
	}
	return resolved;
}

function getImagesIncludePaths(imagesDir, lang) {
	var includePaths = [];
	var partner = '-';
	var prefixes = [
		lang + '/' + partner,
		'-/' + partner,
		lang + '/-',
		'-/-'
	].filter(function(elem, index, arr) {
		return arr.indexOf(elem) === index;
	});
	prefixes.forEach(function(prefix) {
		includePaths.push(imagesDir + prefix + '/i/');
		includePaths.push(imagesDir + prefix + '/css/');
		includePaths.push(imagesDir + prefix + '/js/');
		includePaths.push(imagesDir + prefix + '/');
	});
	return includePaths;
}

function doResolveImagesPath(includePaths, fileName) {
	var filePath;
	var resolved;
	for (var i = 0; i < includePaths.length; i++) {
		filePath = includePaths[i] + fileName;
		if (fs.existsSync(filePath)) {
			resolved =  path.resolve(path.normalize(filePath));
			break;
		}
	}
	return resolved;
}

// Templates
function getBlitzFileContents(tplDir, filePath, fileName) {
	var includeName = fileName;
	var fileNameParts = fileName.match(/^tpl:([^@|]*)(?:@([^|]*))?(?:\|(.*?))?$/);
	fileName = fileNameParts[1];
	var tplName = fileName.replace(/(\.tpl|\.dust\.html)/, '').replace(/[^\/]*\//, '');
	var block = fileNameParts[2] || '';
	var result = dust.compile('javascript', tplDir + filePath, tplName, block);
	return '/* ' + includeName.replace(/^tpl:/,'') + ' => ' +
		filePath + (block ? ' ' + block : '') + ' */\n' + result;
}

function resolveTemplateFile(templatesDir, fileName, lang) {
	var resolved = null;
	if (lang === '-') {
		lang = 'dev';
	}
	var paths = [
		lang + '/',
		'../_templates/dev/',
		'-/'
	];
	for (var i = 0; i < paths.length; i++) {
		if (fs.existsSync(templatesDir + paths[i] + fileName)) {
			resolved = paths[i] + fileName;
			break;
		}
	}
	return resolved;
}

// Virtual
function getVirtualFileContent(virtualDir, fileName) {
	var filePath = virtualDir + '/' + fileName.replace('virtual:','').replace('?', '__').replace(/[^a-zA-Z 0-9]/g, '_');
	if (fs.existsSync(filePath)) {
		return fs.readFileSync(filePath, 'utf-8');
	}
	return null;
}
